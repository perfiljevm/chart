import {useEffect,useRef, useState} from "react";
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import logo from './logo.svg';
import {TextField, Typography, Button, Divider} from "@material-ui/core"
import './App.css';

function App() {
  const [data, setData] = useState([])
  const regMin = useRef();
  const regMax = useRef();
  const humMin = useRef();
  const humMax = useRef();
  const adsMin = useRef();
  const adsMax = useRef();

  function Calculate () {
    let nextData = [];

  for (let i = 0; i <= 8; i++) {
    const humanInWeek = Math.floor(Math.random() * (parseInt(humMax.current.value)-parseInt(humMin.current.value))) + parseInt(humMin.current.value);
    const regMoneyInWeek = Math.floor(Math.random() * (parseInt(regMax.current.value)- parseInt(regMin.current.value))) + parseInt(regMin.current.value);
    const adsInWeek = Math.floor(Math.random() * (parseInt(adsMax.current.value)-parseInt(adsMin.current.value))) + parseInt(adsMin.current.value);
    let sum = Math.floor(adsInWeek + (humanInWeek * regMoneyInWeek))
      nextData.push({
        "name": `Неделя ${i}`,
        "Заработок с 1 регистрации": regMoneyInWeek,
        "Заработок с доп. транзакций": adsInWeek,
        "Количество регистраций": humanInWeek,
        "Суммарный заработок за неделю": sum
      })
    }
    setData(nextData)
  }



  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        
      </header> */}
      <div style={{width:"600px", height:"500px", display: "flex", justifyContent:"center", alignItems: "center", margin: "auto"}}>
      <LineChart
        width={800}
        height={400}
        data={data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="Заработок с 1 регистрации" stroke="#8884d8" activeDot={{ r: 8 }} />
        <Line type="monotone" dataKey="Заработок с доп. транзакций" stroke="#82ca9d" />
        <Line type="monotone" dataKey="Количество регистраций" label stroke="red" />
        <Line type="monotone" dataKey="Суммарный заработок за неделю" label stroke="purple" />
      </LineChart>
      </div>
        <Typography style={{marginBottom:"10px", marginTop:"10px"}}>Диапазон количества получаемой суммы за регистрацию</Typography>
        <TextField
      id="outlined-secondary"
      label="Минимальное значение"
      inputRef={regMin}
      defaultValue={5}
      variant="outlined"
      color="secondary"
      />
      <TextField
      id="outlined-secondary"
      label="Максимальное значение"
      inputRef={regMax}
      defaultValue={10}
      variant="outlined"
      color="secondary"
      />
      <Typography style={{marginBottom:"10px", marginTop:"10px"}}>Диапазон Количества пользователей в неделю</Typography>
      <TextField
      id="outlined-secondary"
      label="Минимальное значение"
      variant="outlined"
      defaultValue={500}
      inputRef={humMin}
      color="secondary"
      />
      <TextField
      id="outlined-secondary"
      label="Максимальное значение"
      inputRef={humMax}
      defaultValue={5000}
      variant="outlined"
      color="secondary"
      />
      <Typography style={{marginTop:"10px"}}>Диапазон вложений пользователей в неделю с учетом того,</Typography>
      <Typography style={{marginBottom:"10px"}}> что вывод и финасирование будет брать от 2 до 10% от комиссии пользователей и оплату рекламы</Typography>
      <TextField
      id="outlined-secondary"
      label="Минимальное значение"
      inputRef={adsMin}
      defaultValue={0}
      variant="outlined"
      color="secondary"
      />
      <TextField
      id="outlined-secondary"
      label="Максимальное значение"
      inputRef={adsMax}
      defaultValue={1000}
      variant="outlined"
      color="secondary"
      />
      <Divider style={{marginBottom:"10px", marginTop:"10px"}} />
      <Button style={{marginBottom:"10px", marginTop:"10px"}} onClick={Calculate} variant="contained" color="primary">
        Обновить таблицу
      </Button>
    </div>
  );
}

export default App;
